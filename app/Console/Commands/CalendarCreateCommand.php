<?php

namespace App\Console\Commands;

use App\Http\Services\CalendarService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CalendarCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calendar:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление кэша календаря';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new CalendarService();

        if ($result = $service->getPrices()) {
            Cache::forget('calendar');
            Cache::put('calendar', $result);
        } else {
            $this->info('Ошибка получения календаря');
        }

        return 0;
    }
}
