<?php

namespace App\Console\Commands;

use App\Http\Services\CalendarService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CalendarCheckPriceCommannd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calendar:check_price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $calendar = Cache::get('calendar');

        $service = new CalendarService();

        $result = [];
        foreach ($calendar as $key => $destinationDates) {
            $result[$key] = $service->checkPrice($destinationDates);
        }

        Cache::forget('calendar');
        Cache::put('calendar', $result);

        return 0;
    }
}
