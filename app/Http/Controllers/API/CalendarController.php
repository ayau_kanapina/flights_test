<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CalendarGetRequest;
use App\Http\Resources\CalendarResource;
use App\Http\Services\CalendarRequestService;
use App\Http\Services\CalendarService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CalendarController extends Controller
{

    /**
     * @param CalendarGetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(CalendarGetRequest $request): \Illuminate\Http\JsonResponse
    {
        $service = new CalendarService();

        $calendar = $service->getByDestination($request->get('from'), $request->get('to'));

        return response()->json($calendar);
    }
}
