<?php


namespace App\Http\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;


class CalendarRequestService
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $partner;

    /**
     * @var string
     */
    protected $apiVersion;

    /**
     *
     */
    protected const CURRENCY = 'KZT';

    /**
     * CalendarRequestService constructor.
     */
    public function __construct()
    {
        $this->host = env('KIWI_HOST');
        $this->partner = env('KIWI_PARTNER');
        $this->apiVersion = env('KIWI_VERSION');
    }


    /**
     * @param EloquentCollection $destinations
     * @param string $dateFrom
     * @param string $dateTo
     * @return array|bool
     */
    public function flights(EloquentCollection $destinations, string $dateFrom, string $dateTo)
    {
        $queries = [];

        foreach ($destinations as $destination) {
            $key = $destination->departure . '-' . $destination->arrival;

            $queries[$key] = [
                'fly_from'  => $destination->departure,
                'fly_to'    => $destination->arrival,
                'date_from' => $dateFrom,
                'date_to'   => $dateTo,
                'partner'   => $this->partner,
                'curr'      => self::CURRENCY
            ];
        }

        $response = $this->asyncGetRequest('/flights', $queries);

        if (!$response) {
            return false;
        }

        foreach ($response as $key => $value) {
            $list = collect($value['data']);

            //группировка результатов направления по датам
            $groupedList = $list->groupBy(function ($item) {
                return date('Y-m-d', $item['dTime']);
            });

            //сортировка результатов за день по цене
            $sorted = $groupedList->map(function ($item, $key) {
                $collection = collect($item);
                $collection = $collection->map(function ($item) {
                    return [
                        'booking_token' => $item['booking_token'],
                        'price'         => $item['price']
                    ];
                });

                return $collection->sortBy('price');
            });

            //перезапись значения
            $response[$key] = $sorted;
        }

        return $response;
    }


    /**
     * @param Collection $destinationDatesData
     * @param Collection $updatedData
     * @param int $bnum
     * @param int $pnum
     */
    public function checkPrice(Collection $destinationDatesData, Collection &$updatedData, int $bnum = 1, int $pnum=1)
    {
        $queries = [];

        foreach ($destinationDatesData as $key => $date) {
            $min = Arr::first($date);

            $queries[$key] = [
                'v'             => $this->apiVersion,
                'booking_token' => $min['booking_token'],
                'bnum'          => $bnum,
                'pnum'          => $pnum,
                'affily'        => $this->partner,
                'curr'          => self::CURRENCY
            ];
        }

        $response = $this->asyncGetRequest('/api/v0.1/check_flights', $queries);

        $datesToCheck = collect(); //для повторной проверки

        foreach ($response as $key => $item) {
            //случай при невалидном перелете
            //перелет удаляется из опций
            //варианты текущего дня добавляются в $datesToCheck для повторной проверки

            if ($item['flights_invalid']) {
                $destinationDatesData[$key]->shift();
                //$datesToCheck->put($key, $destinationDatesData[$key]);
                $datesToCheck[$key] = $destinationDatesData[$key];
                continue;
            }

            //случай при flights_checked=false
            //варианты добавляются на повторную проверку
            if (!data_get($item, 'flights_checked') || $item['flights_invalid']) {
                $datesToCheck[$key] = $destinationDatesData[$key];
                continue;
            }

            //обновление результируещего массив с подтвержденными токенами и обновленными ценами
            //$updatedData->put($key, $destinationDatesData[$key]);
            if ($item['price_change']) {
                $destinationDatesData[$key][0] = [
                    'booking_token' => $item['booking_token'],
                    'price'         => $item['flights_price']
                ];
            }

            $updatedData[$key] = $destinationDatesData[$key];
        }

        //метод вызывается рекурсивно
        if (!$datesToCheck->isEmpty())
            $this->checkPrice($datesToCheck, $updatedData);
    }


    /**
     * @param string $uri
     * @param array $queries
     * @return array|bool
     */
    protected function asyncGetRequest(string $uri, array $queries)
    {
        if (!$this->host || !$this->partner) {
            Log::error('Данные API не корректны');
            return false;
        }

        $client = new Client([
            'base_uri' => $this->host
        ]);

        $promises = [];

        try {
            //формирование асинхронного запроса
            foreach ($queries as $key => $queryParams) {
                $promises[$key] = $client->getAsync($uri, [
                    'query'           => $queryParams,
                    'connect_timeout' => 30
                ]);
            }

            //получение ответа
            $results = Promise\Utils::settle($promises)->wait();

            $response = [];

            //проверка ответов
            foreach ($results as $key => $result) {

                if ($result['state'] == 'fulfilled') {
                    $response[$key] = json_decode($result['value']->getBody()->getContents(), true);
                }
            }

            return $response;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }
    }
}
