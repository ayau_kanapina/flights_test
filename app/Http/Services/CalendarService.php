<?php


namespace App\Http\Services;


use App\Models\CalendarDestination;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class CalendarService
 * @package App\Http\Services
 */
class CalendarService
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var CalendarRequestService
     */
    protected $requestService;

    /**
     * CalendarService constructor.
     */
    public function __construct()
    {
        $this->requestService = new CalendarRequestService();
    }

    /**
     * @return array|bool
     */
    public function getPrices()
    {
        $destinations = CalendarDestination::select('departure', 'arrival')
            ->get()
            ->mapWithKeys(function ($item) {
                return [$item['departure'] . '-' . $item['arrival'] => $item];
            });
        $now = Carbon::now()->format('d/m/Y');
        $month = Carbon::now()->addMonth()->format('d/m/Y');

        return $this->requestService->flights($destinations, $now, $month);
    }

    /**
     * @param Collection $dates
     * @return Collection
     */
    public function checkPrice(Collection $dates): Collection
    {
        $resultedList = collect();

        $this->requestService->checkPrice($dates, $resultedList);

        return $resultedList;
    }


    /**
     * @param string $departure
     * @param string $arrival
     * @return \Illuminate\Support\Collection
     */
    public function getByDestination(string $departure, string $arrival): Collection
    {
        $calendar = Cache::get('calendar');

        $data = collect(data_get($calendar, $departure . '-' . $arrival, []));

        return $data->map(function ($item) {
            return Arr::first($item)['price'];
        });
    }
}
